# Generated by Django 3.0.4 on 2020-04-13 21:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Coach',
            fields=[
                ('Nombre_coach', models.CharField(max_length=50)),
                ('Email_coach', models.EmailField(max_length=200)),
                ('Edad', models.IntegerField()),
                ('Rut', models.IntegerField(primary_key=True, serialize=False)),
                ('Apodo', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Nomina',
            fields=[
                ('Id_nomina', models.AutoField(primary_key=True, serialize=False)),
                ('Nombre_nomina', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Partido',
            fields=[
                ('Nombre_partido', models.CharField(max_length=50)),
                ('Id_patido', models.AutoField(primary_key=True, serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('Nombre_jugador', models.CharField(max_length=50)),
                ('Apodo', models.CharField(max_length=50)),
                ('Fecha_de_nacimiento', models.DateField()),
                ('Edad', models.IntegerField()),
                ('Rut', models.IntegerField(primary_key=True, serialize=False)),
                ('Email', models.EmailField(max_length=200)),
                ('Estatura', models.FloatField()),
                ('Peso', models.IntegerField()),
                ('posicion_de_juego', models.CharField(choices=[('Ba', 'Base'), ('Es', 'Escolta'), ('Al', 'Alero'), ('Al', 'Ala-Pivot'), ('Pi', 'Pivot')], help_text='jelp, chose one', max_length=2)),
            ],
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('Nombre_team', models.CharField(max_length=50)),
                ('Descripcion_team', models.CharField(max_length=50)),
                ('Logo_team', models.ImageField(upload_to='')),
                ('Codigo_team', models.IntegerField(primary_key=True, serialize=False)),
                ('rut_c', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='polls.Coach')),
            ],
        ),
        migrations.DeleteModel(
            name='Album',
        ),
        migrations.DeleteModel(
            name='Musician',
        ),
        migrations.AddField(
            model_name='player',
            name='codigo_t',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='polls.Team'),
        ),
        migrations.AddField(
            model_name='nomina',
            name='Id_patido',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='polls.Partido'),
        ),
        migrations.AddField(
            model_name='nomina',
            name='Rut',
            field=models.ManyToManyField(help_text='insert root', to='polls.Player'),
        ),
    ]
