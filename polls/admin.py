from django.contrib import admin

# Register your models here.
from .models import Team, Player, Coach, Partido, Nomina 

#admin.site.register(Team)
#@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
	list_display = ('Logo_team', 'Nombre_team')
	search_fields = ['Nombre_team',]
	#list_display = ('Fecha_de_nacimiento',)
#admin.site.register(Player)
#admin.site.register(Player, PlayerAdmin)
#@admin.register(Player)
admin.site.register(Team,TeamAdmin)
class PlayerAdmin(admin.ModelAdmin):
	list_display = ('Fotografia','Nombre_jugador')
	list_filter = ('Nombre_jugador','Fecha_de_nacimiento')
	search_fields = ['Nombre_jugador','Apodo','Rut']

admin.site.register(Player,PlayerAdmin)

admin.site.register(Coach)
admin.site.register(Partido)
admin.site.register(Nomina)

