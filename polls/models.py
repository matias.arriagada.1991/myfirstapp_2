from django.db import models


#Creando modelo Team
class Team(models.Model):
    Nombre_team = models.CharField(max_length=50)
    Descripcion_team = models.CharField(max_length=50)
    Logo_team = models.ImageField(upload_to='pics', blank=True,null=True)
    Codigo_team = models.IntegerField(primary_key=True)
    rut_c = models.ForeignKey('Coach', on_delete = models.SET_NULL,null=True)

    def __str__(self):
    	return self.Nombre_team

    def get_absolute_url(self):
    	return reverse('model-detail-view', args=[str(self.Codigo_team)])



class Player(models.Model):
    Nombre_jugador = models.CharField(max_length=50)
    Apodo = models.CharField(max_length=50)
    Fecha_de_nacimiento = models.DateField()
    Edad = models.IntegerField()
    Rut = models.IntegerField(primary_key=True)
    Email = models.EmailField(max_length=200)
    Estatura = models.FloatField()
    Peso = models.IntegerField()
    Fotografia = models.ImageField(upload_to='pics', blank=True,null=True)
    codigo_t = models.ForeignKey('Team', on_delete = models.SET_NULL, null = True)

    opciones = (
    	('Ba','Base'),
    	('Es','Escolta'),
    	('Al','Alero'),
    	('Al','Ala-Pivot'),
    	('Pi','Pivot'),
    )

    posicion_de_juego = models.CharField(max_length=2,choices=opciones, blank=False, help_text='jelp, chose one')

    def __str__(self):
    	return self.Nombre_jugador

    def get_absolute_url(self):
    	return reverse('model-detail-view', args=[str(self.Rut)])  

   
class Coach(models.Model):
    Nombre_coach = models.CharField(max_length=50)
    Email_coach = models.EmailField(max_length=200)
    Edad = models.IntegerField()
    Rut = models.IntegerField(primary_key=True)
    Apodo = models.CharField(max_length=50)

    def __str__(self):
    	return self.Nombre_coach

    def get_absolute_url(self):
    	return reverse('model-detail-view', args=[str(self.Rut)])    

    
class Partido(models.Model):
    Nombre_partido = models.CharField(max_length=50)
    Id_patido = models.AutoField(primary_key=True)
    
    def __str__(self):
    	return self.Nombre_partido

    def get_absolute_url(self):
    	return reverse('model-detail-view', args=[str(self.Id_patido)])


class Nomina(models.Model):
	Id_nomina = models.AutoField(primary_key=True)
	Id_patido = models.ForeignKey('Partido', on_delete=models.SET_NULL,null=True)
	Rut = models.ManyToManyField('Player',help_text='insert root')
	Nombre_nomina = models.CharField(max_length=50)

	def __str__(self):
		return self.Nombre_nomina

	def get_absolute_url(self):
		return reverse('model-detail-view', args=[str(self.Id_nomina)])